import React from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import { connect } from "react-redux";

import {
  authRoute as rsAuth,
  sessionRoute as rsSession,
  landingRoute as rsLanding,
} from "./config";

import { userChange } from "../redux/actions/userAction";
import LayoutWithSidebar from "../views/layout/LayoutWithSidebar";
import LayoutMain from "../views/layout/Main";
import Error404 from "../views/pages/Error404";

import getToken from "../utils/sessions";

const mapRoutes = (Layout, routes) => {
  if (routes.length) {
    return routes.map(({ path, isWhiteBG, component: Component }, index) => (
      <Route
        key={String(index)}
        path={path}
        exact
        render={(props) => (
          <Layout>
            <Component {...props} isWhiteBG={isWhiteBG} />
          </Layout>
        )}
      />
    ));
  }
};

class Routes extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const token = getToken();
    return (
      <Router>
        <Switch>
          <Route
            exact
            path="/"
            render={(t) => (t ? <Redirect to="/home" /> : <Redirect to="/landing" />)}
          />
          {mapRoutes(LayoutWithSidebar, rsLanding)}
          {!token ? (
            mapRoutes(LayoutMain, rsSession)
          ) : mapRoutes(LayoutWithSidebar, rsAuth) }
          <Route render={() => <Error404 />} />
        </Switch>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

export default connect(mapStateToProps, { userChange })(Routes);
