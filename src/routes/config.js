import LandingPage from "../views/pages/LandingPage";
import LoginPage from "../views/pages/LoginPage";
import RegisterPage from "../views/pages/RegisterPage";
import ForgetPasswordPage from "../views/pages/ForgetPasswordPage";
import HomePage from "../views/pages/HomePage";

export const authRoute = [
  {
    path: "/login",
    name: "Login",
    component: LoginPage,
    isWhiteBG: false,
  },
];

export const sessionRoute = [
  {
    path: "/home",
    name: "Dashboard",
    component: HomePage,
    isWhiteBG: false,
  },
];

export const landingRoute = [
  {
    path: "/landing",
    name: "Landing",
    component: LandingPage,
    isWhiteBG: false,
  },
  {
    path: "/register",
    name: "RegisterPage",
    component: RegisterPage,
    isWhiteBG: false,
  },
  {
    path: "/forget-password",
    name: "ForgetPasswordPage",
    component: ForgetPasswordPage,
    isWhiteBG: false,
  },
];
export default { authRoute, sessionRoute, landingRoute };
