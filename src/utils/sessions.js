const token = "token";

const getToken = () => localStorage.getItem(token) || null;
const setToken = (value) => localStorage.setItem(token, value);

export default getToken;
export { setToken };
