import * as types from "../config";

export const init = (data) => {
  return {
    type: types.INIT,
    data,
  };
};

export const showLoading = () => {
  return ({
    type: types.SHOW_LOADING,
    isLoading: true,
  });
};
export const hideLoading = () => {
  return ({
    type: types.HIDE_LOADING,
    init: false,
  });
};
