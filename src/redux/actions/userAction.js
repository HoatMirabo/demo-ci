import * as types from "../config";

export const userChange = (user) => {
  return {
    type: types.USERCHANGE,
    user,
  };
};

export const userClear = () => {
  return {
    type: types.USERCLEAR,
  };
};
