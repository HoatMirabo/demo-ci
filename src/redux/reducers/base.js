import * as types from "../config";

const initialState = {};
export default (state = initialState, actions) => {
  switch (actions.type) {
    case types.INIT:
      return { ...state, init: actions.data };
    default:
      return state;
  }
};
