import React from "react";
import { Provider } from "react-redux";
import { CookiesProvider } from "react-cookie";
import Main from "./views/Main";
import configureStore from "./redux/store";

const store = configureStore();
export default function App() {
  // const [phone,setPhoneNumber] = useState(getPhone())
  return (
    <CookiesProvider>
      <Provider store={store}>
        <Main />
      </Provider>
    </CookiesProvider>
  );
}
