import React from "react";

import "../../../public/css/dice.css";

export default class dice extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      r1: {
        transform: "rotateX(0deg) rotateY(90deg)",
      },
      r2: {
        transform: "rotateX(90deg) rotateY(360deg)",
      },
      r3: {
        transform: "rotateX(270deg) rotateY(0deg)",
      },
      randX: [],
      randY: [],
    };
    this.getRandom = this.getRandom.bind(this);
    this.handleRoll = this.handleRoll.bind(this);
  }

  componentDidUpdate() {
    this.props.handleRoll(this.state);
  }

  handleRoll() {
    const min = 1;
    const max = 24;
    const randX = this.getRandom(max, min);
    const randY = this.getRandom(max, min);
    const rollState = {
      r1: {
        transform: `rotateX(${randX[0]}deg) rotateY(${randY[0]}deg)`,
      },
      r2: {
        transform: `rotateX(${randX[1]}deg) rotateY(${randY[2]}deg)`,
      },
      r3: {
        transform: `rotateX(${randX[2]}deg) rotateY(${randY[2]}deg)`,
      },
      randX,
      randY,
    };
    this.setState(rollState);
  }

  getRandom(max, min) {
    return [
      (Math.floor(Math.random() * (max - min)) + min) * 90,
      (Math.floor(Math.random() * (max - min)) + min) * 90,
      (Math.floor(Math.random() * (max - min)) + min) * 90,
    ];
  }

  render() {
    return (
      <>
        <div className="row mb-5">
          <div className="col-sm">
            <section className="container">
              <div id="cube" style={this.state.r1}>
                <div className="front" />
                <div className="back" />
                <div className="right" />
                <div className="left" />
                <div className="top" />
                <div className="bottom" />
              </div>
            </section>
          </div>
          <div className="col-sm">
            <section className="container">
              <div id="cube" style={this.state.r2}>
                <div className="front" />
                <div className="back" />
                <div className="right" />
                <div className="left" />
                <div className="top" />
                <div className="bottom" />
              </div>
            </section>
          </div>
          <div className="col-sm">
            <section className="container">
              <div id="cube" style={this.state.r3}>
                <div className="front" />
                <div className="back" />
                <div className="right" />
                <div className="left" />
                <div className="top" />
                <div className="bottom" />
              </div>
            </section>
          </div>
        </div>
        <button
          onClick={() => {
            this.handleRoll();
          }}
          className="learn-more mt-5"
        >
          Quay

        </button>
      </>
    );
  }
}
