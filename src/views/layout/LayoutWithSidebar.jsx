import React from "react";
import BackgroundImage from "../../../public/image/background.png";

const HeaderStyle = {
  width: "100%",
  height: "100vh",
  background: `url(${BackgroundImage})`,
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundSize: "cover",
};

function objectWithoutProperties(obj, keys) {
  const target = {};
  for (const i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }
  return target;
}

export default class LayoutWithSidebar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { children } = this.props;
    const childrenProps = Object.assign(children.props);
    let layout = objectWithoutProperties(children, "props");
    layout = Object.assign(layout, { props: childrenProps });
    return (
      <div className="p-5 auto" style={HeaderStyle}>
        {layout}
      </div>
    );
  }
}
