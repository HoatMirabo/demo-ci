import React from "react";
import classNames from "classnames";

function DivMain({ className, children }) {
  return <div className={classNames("main", className)}>{children}</div>;
}

function Main({ children }) {
  return <DivMain>{children}</DivMain>;
}

export default Main;
