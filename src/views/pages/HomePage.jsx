import React, { useEffect, useState } from "react";
import Dice from "../component/dice";

export default function HomePage() {
  const [roll, setRoll] = useState([]);
  useEffect(() => {
    console.log(roll);
  });
  return (
    <div className="text-center">
      <h1 className="main-title home-page-title">welcome to our app</h1>
      <Dice handleRoll={(obj) => { setRoll(obj); }} />
    </div>
  );
}
