#!/bin/bash

DEPLOY_SERVER=$DEPLOY_SERVER
SERVER_FOLDER="git"
STATIC_PATH="/home/test-ci"

# Building React output
npm install
npm run build:start
# copy file to sever

echo "Deploying to ${DEPLOY_SERVER}"
echo "root@${DEPLOY_SERVER}:${STATIC_PATH}/${SERVER_FOLDER}/"
scp -r dist/ ${DEPLOY_SERVER}:${STATIC_PATH}/${SERVER_FOLDER}/

echo "Finished copying the build files"
