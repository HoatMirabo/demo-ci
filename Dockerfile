FROM node:16-alpine3.11

RUN mkdir /app
WORKDIR /app

# install simple http server for serving static content
RUN npm install -g http-server

RUN apk add yarn
# copy both 'package.json' and 'package-lock.json' (if available)
COPY ./package*.json /app/

# install project dependencies
RUN yarn install

# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . /app/

CMD [ "yarn", "runDev" ]
